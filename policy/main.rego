package main

MANDATORY_TAGS = ["owner", "Name"]
VPC_NAMING_CONVENTION = "[a-z_]{10}"

hash_contains_keys(hash, item) {
	_ = hash[key]
	key = item
}

# VPC should have a Name tag that follows naming convention
deny[msg] {
        resource := input.resource_changes[_]
        resource.type == "aws_vpc"
        resource_name := resource.change.after.tags.Name
        not re_match(VPC_NAMING_CONVENTION, resource_name)
	msg = sprintf("[DENY] resource %s does not match the corporate naming convention", [resource_name])
}

# Check that resource has the expected tags
deny[msg] {
	resource := input.resource_changes[_]
	required_tag := MANDATORY_TAGS[_]
	not hash_contains_keys(resource.change.after.tags, required_tag)
	msg = sprintf("[DENY] %s should have tag '%s'", [resource.name, required_tag])
}
